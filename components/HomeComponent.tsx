import React, {Component} from "react";
import {Button, Switch, Text, View} from "react-native";
import firebase from "../config/firebase/firebase";
import {themes} from "../theme/Themes";

export class HomeComponent extends Component {
    private db: any;
    private onSnapshotUnsubscribe: any;
    state = {
        displayName: "",
        userId: "",
        isSwitched: false,
        theme: themes.light
    };

    constructor(props: any) {
        super(props);
        this.db = firebase.firestore();
    }

    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                alignContent: 'center',
                justifyContent: 'center',
                backgroundColor: this.state.theme.backgroundColor
            }}>
                <Text style={{
                    fontWeight: "800",
                    fontSize: 30,
                    color: this.state.theme.textColor
                }}>Welcome, {this.state.displayName}</Text>
                <Button title={"Sign Out"} onPress={() => {
                    this.onPressSignOut()
                }}/>
                <Switch onValueChange={() => {
                    this.onToggleSwitch()
                }}
                        value={this.state.isSwitched}
                />
            </View>

        );
    }

    private onPressSignOut(): void {
        firebase.auth().signOut().then(() => {
            this.props.navigation.reset({
                index: 0,
                routes: [{name: "Sign in"}],
            });
        })
            .catch((error: Error) => this.setState({errorMessage: error.message}))
    }

    private onToggleSwitch() {
        this.db
            .collection("app-db")
            .doc(this.state.userId).set({
            isThemeSwitched: !this.state.isSwitched
        });
    }

    componentWillMount() {
        this.setState({displayName: firebase.auth().currentUser.displayName})
        this.setState({userId: firebase.auth().currentUser.uid})
    }

    componentDidMount() {
        this.onSnapshotUnsubscribe =
            this.db
                .collection("app-db")
                .doc(this.state.userId)
                .onSnapshot((snapshot: any) => {
                    const data = snapshot.data()
                    if (data != undefined) {
                        this.setState({
                            isSwitched: data.isThemeSwitched as boolean
                        })
                    }

                    if (this.state.isSwitched) {
                        this.setState({theme: themes.dark})
                    } else {
                        this.setState({theme: themes.light})
                    }
                });

        this.db.collection("app-db")
            .doc(this.state.userId)
            .get()
            .then((snapshot: any) => {
                const data = snapshot.data()
                if (data === undefined) {
                    this.db
                        .collection("app-db")
                        .doc(this.state.userId).set({
                        isThemeSwitched: false
                    });
                } else {
                    this.setState({
                        isSwitched: snapshot.data().isThemeSwitched as boolean
                    });
                }
            });
    }

    componentWillUnmount() {
        this.onSnapshotUnsubscribe();
    }
}
